# Необходим для того, чтобы работать с запросами. Обращение к сайту и далее вытягивать информацию
import requests
# Разбирать HTML страницу. Делать из неё объект. И Далее работать с этим объектом.
from bs4 import BeautifulSoup
import csv

CSV = 'cards.csv'
HOST = 'https://minfin.com.ua'
URL = 'https://minfin.com.ua/cards/' # Страница которую парсим.

# Для того, чтобы сайты не подумали, что мы какой-то скрипт или бот, необходимо пробрасывать заголовки.
HEADERS = {
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 YaBrowser/21.9.2.169 Yowser/2.5 Safari/537.36'
}

# Функция для получения HTML
def get_html(url, params=''):
    r = requests.get(url, headers=HEADERS, params=params)
    return r

# Функция, которая получает контент
def get_content(html):
    soup = BeautifulSoup(html, 'html.parser')
    # парсим конктретные блоки
    items = soup.find_all('div', class_='bfjox4-1')
    cards = []

    for item in items:
        cards.append(
            {
                'title': item.find('div', class_='bfjox4-15').find(class_='sc-6nr3q5-0').get_text(strip=True),
                'link_product': HOST + item.find('div', class_='bfjox4-15').find(class_='sc-6nr3q5-0').get('href'),
                'Name_bank': item.find('div', class_='bfjox4-16').find(class_='bfjox4-35').get_text(strip=True),
                'card_img': item.find('div', class_='bfjox4-9').find('img').get('src')
            }
        )
    return cards

html = get_html(URL)
print(get_content(html.text))

def save_doc(items, path):
    with open(path, 'w', newline='', encoding='utf-16') as file:
        writer = csv.writer(file, delimiter=';')
        writer.writerow(['Название продукта', 'Ссылка на продукт', 'Банк', 'Изображение карты'])
        for item in items:
            writer.writerow([item['title'], item['link_product'], item['Name_bank'], item['card_img']])


def parser():
    html = get_html(URL)
    if html.status_code == 200:
        print("Идет процесс парсинга...")
        cards = get_content(html.text)
        save_doc(cards, CSV)
    else:
        print('Error')

parser()



