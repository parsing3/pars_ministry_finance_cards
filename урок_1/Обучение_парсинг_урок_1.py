from bs4 import BeautifulSoup
import re
# Открываем файл нашей страницы
with open("index.html", encoding='utf-8') as file:
    src = file.read()

# Скармливаем наш скрипт кода страницы Beatifulsoup, чтобы мы могли пользоваться методами библиотеки для извлеч инф
# Более сложно. Преобразовать код в дерово объектов пайтон.

soup = BeautifulSoup(src, "lxml")

title = soup.title
print(title.text)

# .find()будет искать определенный тег по всей страницы. Возьмет первый попавшийся
# .find_all() - Отберет все теги на странице. Вернет списком.
page_h1 = soup.find('h1')
print(page_h1.text)
page_h1_all = soup.find_all("h1")
print(page_h1_all)

for item in page_h1_all:
   print(item.text, "\n")

# Можно конкретизировать тег указывая к какому атрибуту отсносится тег.
user_name = soup.find("div", class_="user__name")
print(user_name) # Получаем класс с тегом
print(user_name.text.strip()) # strip удалит пробелы с двух сторон.

# Можно шагать в глубь по коду. Сначало нашли класс. Потом в этом классе тег.
user_name = soup.find("div", class_="user__name").find("span")
print(user_name.text)

# Фильтрация поиска -> передача словаря. Например у класса есть id. И мы хотим именно
# этот класс с данным id или его определенным признаком.
user_name = soup.find("div", {"class":"user__name", "id":"aaa"}).find("span").text
print(user_name, "\n")

# Берем класс "user__info". И достаем от дута имя, дату рождения и город
find_all_spans_in_user__info = soup.find("div", class_="user__info").find_all("span")
for item in find_all_spans_in_user__info:
    print(item.text)

social_links = soup.find("div", class_="social__networks").find("ul").find_all("a")
print(social_links)

for item in social_links:
    item_url = item.get("href")
    print("{}: {} \n".format(item.text, item_url))

# Для перемещения по дом дереву: .find.parent(), .find.parants() ищут родителей элементов
# То есть поднимаются снизу вверх.
post_div = soup.find(class_="post__text").find_parent("div", "user__post")
print(post_div)

# .next_element, .previous_element
# next_element прописываем два раза, так как следующая - это переход строки.
next_el = soup.find(class_="post__title").next_element.next_element
print(next_el.text)

next_el_1 = soup.find(class_= "post__title").find_next().text
print(next_el_1)

# .find_next_sibling(), .find_previous_sibling() - ищут и возвращают следующий и предыдущие
# элементы внутри искомого тега
# Например div_1, следующий ищет div_2. Если даже после div_1 стоит h1,
# будет искать следующий div
next_sib = soup.find(class_="post__title").find_next_sibling()
print(next_sib)

previous_sib = soup.find(class_="post_date").find_previous_sibling().text
print(previous_sib)
# ___________________________


links = soup.find(class_="some__links").find_all("a")
print(links)

for item in links:

    link_href_attr = item.get("href")
    link_href_attr_1 = item["href"]

    link_data_attr = item.get("data-attr")
    link_data_attr_1 = item["data-attr"]

    print(link_href_attr)
    print(link_data_attr)

# Если необходимо искать по отдельным словам из содержания
find_a_by_text = soup.find("a", text=re.compile("Одежда"))
print(find_a_by_text)

# Если мы хотим по слову одежда и Одежда по всему скрипту сайта
find_all_clothes = soup.find_all(text=re.compile("([Оо]дежда)"))
print(find_all_clothes)


post_text = soup.find(class_="post__text")
print("\n")
print(post_text.text.strip())
