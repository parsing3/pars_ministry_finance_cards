import requests
from bs4 import BeautifulSoup
import json

"""
# Ссылка на нашу страницу, которую хотим спарсить
url = "https://health-diet.ru/table_calorie/?utm_source=leftMenu&utm_medium=table_calorie"

headers = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.85 YaBrowser/21.11.0.1996 Yowser/2.5 Safari/537.36"
}

# req возвращает результат работы метода .get
req = requests.get(url, headers=headers)
# Получили код страницы
src = req.text
# print(src)

# Сохраняем получившую страницу в файл
with open("index.html", 'w', encoding = "utf-8") as file:
    file.write(src)"""

"""# Прочитаем файл и сохраним в переменную
with open("index.html", encoding = "utf-8") as file:
    src = file.read()

# создадим объект супа и передадим нашу переменну src, парсер lxml
soup = BeautifulSoup(src, "lxml")


# Первое. Возьмем заголовки и ссылки. К ссылкам добавили доменное имя
all_products_hrefs = soup.find_all(class_="mzr-tc-group-item-href")

all_categories_dict = {}
for item in all_products_hrefs:
    item_text = item.text
    item_href = "https://health-diet.ru" + item.get("href")

    all_categories_dict[item_text] = item_href

with open("all_categories_dict.json", "w") as file:
    # indent делает отступы, ensure_ascii - меняет в читабельную кодировку
    json.dump(all_categories_dict, file, indent = 4, ensure_ascii=False"""


with open("all_categories_dict.json") as file:
    all_categories = json.load(file)

# Созаем цикл при каждой итерации будет заходить в каждую категорию и собирать все данные
# с товаров и химическом составе и записывать все это дело в файл
for category_name, cotegory_href in all_categories.items():
    # Заменяем запятыеб пробелы и т.д. на нижнее подчеркивание
    rep = [",", " ", "-", "'"]
    for item in rep:
        if item in category_name:
            category_name = category_name.replace(item, "_")
    print(category_name)


